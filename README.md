# What is this?

This is a fork of the [C lolcat implementation](https://github.com/jaseg/lolcat) with changes made to allow it to compile in the IBM i PASE environment. 

# What does it do?

lolcat works similarly to the `cat` command, but when it writes to standard out, the text is rainbow-colored.
   
![](https://bitbucket.org/ataylorkt/lolcat-c/raw/master/screenshot.png)

Details about the command can be found by running `lolcat --help`

![](https://bitbucket.org/ataylorkt/lolcat-c/raw/master/help.png)

# Installation

You can build lolcat yourself. It has been successfully built in IBM i 7.1 PASE using: 

* [gcc 4.8.2 built for AIX V5.3](http://www.perzl.org/aix/index.php?n=Main.Gcc)
* [GNU make 4.2.1 built for AIX V5.3](http://www.perzl.org/aix/index.php?n=Main.Make)

```bash
$ make lolcat
```

Afterwards, you can move the `lolcat` binary to your directory of choice, such as `$HOME/bin` or `/QOpenSys/usr/bin` if you want everyone on your IBM i to share the joy.

# License
WTFPL License. See file `LICENSE` for details.